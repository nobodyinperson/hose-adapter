/* [Fittings] */
// Outer diameter(s) of the hose adapters on the input side
fitting_diameter = 9;            // [1:0.001:30]
fitting_wall_thickness = 2;      // [0.1:0.01:5]
fitting_length = 40;             // [10:0.1:100]
fitting_barb_extra_diameter = 2; // [0:0.1:10]
fitting_barb_count = 4;          // [1:1:10]

/* [Junction] */
junction_angle = 30;   // [1:1:180]
junction_distance = 8; // [0:0.1:50]
junction_height = junction_distance / tan(junction_angle / 2);
junction_separately = true;

/* [Precision] */
$fn = 50;

/* [Tubes (only for demonstration)] */
tube_wall_thickness = 3; // [0.1:0.1:10]
tube_length = 60;        // [10:0.1:100]

module tube(diameter, wall_thickness, length)
{
  difference()
  {
    cylinder(r = diameter / 2, h = length);
    expand = 0.1;
    translate([ 0, 0, -length * expand / 2 ])
      cylinder(r = diameter / 2 - wall_thickness, h = length * (1 + expand));
  }
}

module barbed_tube(diameter = fitting_diameter,
                   wall_thickness = fitting_wall_thickness,
                   length = fitting_length,
                   barb_diameter = fitting_diameter +
                                   fitting_barb_extra_diameter,
                   barb_count = fitting_barb_count)
{
  barb_length = length / barb_count;
  difference()
  {
    union()
    {
      for (i = [0:(barb_count - 1)])
        translate([ 0, 0, i * barb_length ])
          cylinder(d1 = barb_diameter, d2 = diameter, h = barb_length);
    }
    expand = 0.05;
    translate([ 0, 0, -barb_count * barb_length * expand / 2 ])
      cylinder(d = diameter - 2 * wall_thickness,
               h = barb_count * barb_length * (1 + expand));
  }
}

difference()
{
  union()
  {
    // the three barbed input/output tube fittings
    translate([ junction_distance, 0, junction_height / 2 ])
      rotate([ 0, junction_angle / 2, 0 ]) barbed_tube();
    translate([ -junction_distance, 0, junction_height / 2 ])
      rotate([ 0, -junction_angle / 2, 0 ]) barbed_tube();
    translate([ 0, 0, -junction_height / 2 ]) rotate([ 180, 0, 0 ])
      barbed_tube();

    difference()
    {
      // the junction body
      hull()
      {
        translate([ junction_distance, 0, junction_height / 2 ])
          rotate([ 0, junction_angle / 2, 0 ]) cylinder(
            d = fitting_diameter + fitting_barb_extra_diameter, h = 1e-30);
        translate([ -junction_distance, 0, junction_height / 2 ])
          rotate([ 0, -junction_angle / 2, 0 ]) cylinder(
            d = fitting_diameter + fitting_barb_extra_diameter, h = 1e-30);
        translate([ 0, 0, -junction_height / 2 ]) rotate([ 180, 0, 0 ])
          cylinder(d = fitting_diameter + fitting_barb_extra_diameter,
                   h = 1e-30);
      }

      if (junction_separately) {
        hull()
        {
          translate([ junction_distance, , 0, junction_height / 2 ])
            rotate([ 0, junction_angle / 2, 0 ]) cylinder(
              d = fitting_diameter - 2 * fitting_wall_thickness, h = 1e-30);
          translate([ 0, 0, -junction_height / 2 ]) rotate([ 180, 0, 0 ])
            cylinder(d = fitting_diameter - 2 * fitting_wall_thickness,
                     h = 1e-30);
        }
        hull()
        {
          translate([ -junction_distance, 0, junction_height / 2 ])
            rotate([ 0, -junction_angle / 2, 0 ]) cylinder(
              d = fitting_diameter - 2 * fitting_wall_thickness, h = 1e-30);
          translate([ 0, 0, -junction_height / 2 ]) rotate([ 180, 0, 0 ])
            cylinder(d = fitting_diameter - 2 * fitting_wall_thickness,
                     h = 1e-30);
        }
      } else {
        // remove insides to connect fittings
        hull()
        {
          translate([ junction_distance, 0, junction_height / 2 ])
            rotate([ 0, junction_angle / 2, 0 ]) cylinder(
              d = fitting_diameter - 2 * fitting_wall_thickness, h = 1e-30);
          translate([ -junction_distance, 0, junction_height / 2 ])
            rotate([ 0, -junction_angle / 2, 0 ]) cylinder(
              d = fitting_diameter - 2 * fitting_wall_thickness, h = 1e-30);
          translate([ 0, 0, -junction_height / 2 ]) rotate([ 180, 0, 0 ])
            cylinder(d = fitting_diameter - 2 * fitting_wall_thickness,
                     h = 1e-30);
        }
      }
    }
  }
  // cut it open to show flow path
  if ($preview) {
    translate([ 0, -fitting_diameter / 2, 0 ]) cube(
      [
        (fitting_length * 2 + junction_height) * 1.5,
        fitting_diameter,
        (fitting_length * 2 + junction_height) * 1.5
      ],
      center = true);
  }
}

// show tubes
if ($preview) {
  color("#ffffff", 0.7)
  {

    translate([ junction_distance, 0, junction_height / 2 ])
      rotate([ 0, junction_angle / 2, 0 ])
        tube(diameter = fitting_diameter + 2 * tube_wall_thickness,
             wall_thickness = tube_wall_thickness,
             length = tube_length);
    translate([ -junction_distance, 0, junction_height / 2 ])
      rotate([ 0, -junction_angle / 2, 0 ])
        tube(diameter = fitting_diameter + 2 * tube_wall_thickness,
             wall_thickness = tube_wall_thickness,
             length = tube_length);
    translate([ 0, 0, -junction_height / 2 ]) rotate([ 180, 0, 0 ])
      tube(diameter = fitting_diameter + 2 * tube_wall_thickness,
           wall_thickness = tube_wall_thickness,
           length = tube_length);
  }
}

